birthdays = {
	'アリス': '4/1',
	'ボブ': '12/12',
	'キャロル': '4/4'
}

while True:
	name = input('名前を入力してください：(終了するにはEnterだけ押してください)')
	if name == '':
		break
		
	if name in birthdays:
		print(name + 'の誕生日は' + birthdays[name])
	else:
		print(name + 'の誕生日は未登録です。')
		bday = input('誕生日を入力してください：')
		birthdays[name] = bday
		print('誕生日データベースを更新しました。')
