#! python3
# pw.py - パスワード管理プログラム(脆弱性有り)

PASSWORDS = {'email': 'asdfasdf',
						 'blog': 'qwerqwer',
						 'luggage': '12345'}
						 
import sys
if len(sys.argv) < 2:
	print('使い方	: python pw.py [アカウント名]')
	print('パスワードをクリップボードにコピーします')
	
account = sys.argv[1] # 最初のコマンドライン引数がアカウント名

if account in PASSWORDS:
	pyperclip.copy(PASSWORDS[account])
	print(account + 'のパスワードをクリップボードにコピーしました')
else:
	print(account + 'というアカウント名はありません')
