import random

num = random.randint(1, 20)
count = 0

print('1から20までの数を当ててください。')
while True:
	input_num = input('数を入力してください。')
	count += 1
	if int(input_num) < num:
		print('小さいです。')
	elif int(input_num) > num:
		print('大きいです。')
	elif int(input_num) == num:
		print('当たり！' + str(count) + '回で当たりました！')
		break
			
